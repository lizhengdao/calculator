package com.example.calculatorapplication;

public enum Operation {

    PLUS,
    MINUS,
    PERCENT,
    MULTI,
    DIV,
    UNDEFINED;
}

package com.example.calculatorapplication;

import static java.lang.Math.abs;

public class Calculator {

    private Double n1, n2;
    private Operation op = Operation.UNDEFINED;

    public void setFirstOperand(Double number) {
        n1 = number;
    }

    public void setSecondOperand(Double number) {
        n2 = number;
    }

    public boolean isFirstOperand() {
        return n1 != null;
    }

    public boolean isSecondOperand() {
        return n1 != null;
    }

    public void setOp(Operation op) {
        this.op = op;
    }

    public Double equals() {
        switch (op) {
            case PLUS:
                return n1 + n2;
            case MINUS:
                return n1 - n2;
            case PERCENT:
                return abs(n1/100);
            case MULTI:
                return n1 * n2;
            case DIV:
                if (n2 != 0) {
                    return  n1 / n2;
                } else throw new ArithmeticException("Деление на ноль!");
            default:
                throw new IllegalStateException("Unexpected value: " + op);
        }
    }

    public void clear() {
        n1 = n2 = null;
        op = Operation.UNDEFINED;
    }
}

package com.example.calculatorapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private NumberBuilder numberBuilder = new NumberBuilder(9);
    private Calculator calculator = new Calculator();
    private boolean isBuildingNumber = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpBtnNumbers();
        setUpCalculator();
    }

    private void setUpBtnNumbers() {
        findViewById(R.id.one).setOnClickListener((e) -> {
            if (numberBuilder.addNumber(1)) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.two).setOnClickListener((e) -> {
            if (numberBuilder.addNumber(2)) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.three).setOnClickListener((e) -> {
            if (numberBuilder.addNumber(3)) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.four).setOnClickListener((e) -> {
            if (numberBuilder.addNumber(4)) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.five).setOnClickListener((e) -> {
            if (numberBuilder.addNumber(5)) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.six).setOnClickListener((e) -> {
            if (numberBuilder.addNumber(6)) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.seven).setOnClickListener((e) -> {
            if (numberBuilder.addNumber(7)) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.eight).setOnClickListener((e) -> {
            if (numberBuilder.addNumber(8)) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.nine).setOnClickListener((e) -> {
            if (numberBuilder.addNumber(9)) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.zero).setOnClickListener((e) -> {
            if (numberBuilder.addNumber(0)) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.point).setOnClickListener((e) -> {
            if (numberBuilder.addPoint()) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.sign).setOnClickListener((e) -> {
            numberBuilder.changeSign();
            ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
        });
        findViewById(R.id.delSymb).setOnClickListener((e) -> {
            if (numberBuilder.remove()) {
                isBuildingNumber = true;
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
        findViewById(R.id.ac).setOnClickListener((e) -> {
            numberBuilder.clear();
            calculator.clear();
            ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
        });
    }

    private void setUpCalculator() {
        findViewById(R.id.equals).setOnClickListener((e) -> {
            if (calculator.isFirstOperand()) {
                calculator.setSecondOperand(numberBuilder.buildNumber().doubleValue());
                numberBuilder.clear();
                try {
                    numberBuilder.parseFromNumber(calculator.equals());
                    numberBuilder.tryParseToInteger();
                    calculator.clear();
                    ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
                } catch (ArithmeticException ex) {
                    calculator.clear();
                    ((TextView) findViewById(R.id.textView)).setText(ex.getMessage());
                }
            }
        });

        findViewById(R.id.multiply).setOnClickListener((e) -> {
            if (!isBuildingNumber || !calculator.isFirstOperand()) {
                isBuildingNumber = false;
                calculator.setOp(Operation.MULTI);
                if (!calculator.isFirstOperand()) {
                    calculator.setFirstOperand(numberBuilder.buildNumber().doubleValue());
                }
                numberBuilder.clear();
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });

        findViewById(R.id.plus).setOnClickListener((e) -> {
            if (!isBuildingNumber || !calculator.isFirstOperand()) {
                isBuildingNumber = false;
                calculator.setOp(Operation.PLUS);
                if (!calculator.isFirstOperand()) {
                    calculator.setFirstOperand(numberBuilder.buildNumber().doubleValue());
                }
                numberBuilder.clear();
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });

        findViewById(R.id.minus).setOnClickListener((e) -> {
            if (!isBuildingNumber || !calculator.isFirstOperand()) {
                isBuildingNumber = false;
                calculator.setOp(Operation.MINUS);
                if (!calculator.isFirstOperand()) {
                    calculator.setFirstOperand(numberBuilder.buildNumber().doubleValue());
                }
                numberBuilder.clear();
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });

        findViewById(R.id.div).setOnClickListener((e) -> {
            if (!isBuildingNumber || !calculator.isFirstOperand()) {
                isBuildingNumber = false;
                calculator.setOp(Operation.DIV);
                if (!calculator.isFirstOperand()) {
                    calculator.setFirstOperand(numberBuilder.buildNumber().doubleValue());
                }
                numberBuilder.clear();
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });

        findViewById(R.id.proc).setOnClickListener((e) -> {
            if (!calculator.isFirstOperand()) {
                calculator.setOp(Operation.PERCENT);
                calculator.setFirstOperand(numberBuilder.buildNumber().doubleValue());
                numberBuilder.parseFromNumber(calculator.equals());
                numberBuilder.tryParseToInteger();
                calculator.clear();
                ((TextView) findViewById(R.id.textView)).setText(numberBuilder.toStringNumber());
            }
        });
    }
}
package com.example.calculatorapplication;

import java.util.ArrayList;
import java.util.List;

public class NumberBuilder {

    private int size;
    private final int MAX_SIZE;
    private boolean pointRequirement;
    private boolean isDouble;
    private boolean isNegative;
    private boolean isZero;
    private List<Node> nodes;

    public NumberBuilder(int max_size) {
        MAX_SIZE = max_size;
        nodes = new ArrayList<>();
        isDouble = false;
        isNegative = false;
        pointRequirement = false;
        isZero = true;
        size = 0;
    }

    public boolean remove() {
        if (size == 0) {
            return false;
        }
        int index = nodes.size() - 1;
        nodes.remove(index);
        if (nodes.size() > 1) {
            if (nodes.get(index - 1).getType() == TypeNode.POINT) {
                nodes.remove(index - 1);
                isDouble = false;
            }
        }
        if (nodes.size() == 0) {
            isZero = true;
            isNegative = false;
        } else if (nodes.size() == 1) {
            if (nodes.get(0).getSymbolic() == '0') {
                nodes.remove(0);
                size--;
                isZero = true;
                isNegative = false;
            }
        }
        size--;
        return true;
    }

    public void tryParseToInteger() {
        int index = 0;
        int pos = 0;
        if (isDouble) {
            for (;index < nodes.size(); index++) {
                if (nodes.get(index).getType() == TypeNode.POINT) {
                    pos = index;
                    break;
                }
            }
            index += 1;
            for (;index < nodes.size(); index++) {
                if (nodes.get(index).getSymbolic() != '0') {
                    return;
                }
            }
            isDouble = false;
            while (pos < nodes.size()) {
                nodes.remove(pos);
            }
            if (buildNumber().intValue() == 0) {
                clear();
            }
        }
    }

    public void clear() {
        nodes = new ArrayList<>();
        size = 0;
        isDouble = false;
        isNegative = false;
        pointRequirement = false;
        isZero = true;
    }

    public boolean addNumber(int n) {
        if (size >= MAX_SIZE)
            return false;
        if (isZero && n == 0 && !pointRequirement) {
            return false;
        }
        NumberNode node = new NumberNode(n);
        if (pointRequirement) {
            if (size == 0) {
                nodes.add(new NumberNode(0));
                size++;
            }
            nodes.add(new PointNode());
            isDouble = true;
            pointRequirement = false;
        }
        nodes.add(node);
        size++;
        isZero = false;
        return true;
    }

    public boolean addPoint() {
        if (isDouble) {
            return false;
        } else {
            pointRequirement = true;
            return true;
        }
    }

    public String toStringNumber() {
        if (size == 0) return "0";
        StringBuilder sb = new StringBuilder(size + 1);
        if (isNegative) {
            sb.append('-');
        }
        for (int i = 0; i < nodes.size(); i++) {
            sb.append(nodes.get(i).getSymbolic());
        }
        return sb.toString();
    }

    public Number buildNumber() {
        if (isDouble) {
            return Double.parseDouble(toStringNumber());
        } else {
            return Integer.parseInt(toStringNumber());
        }
    }

    public boolean isDouble() {
        return isDouble;
    }

    public void parseFromNumber(Number n) {
        clear();
        String s = n.toString();
        for (int i = 0; i < s.length(); i++) {
            switch (s.charAt(i)) {
                case '0':
                    addNumber(0);
                    break;
                case '1':
                    addNumber(1);
                    break;
                case '2':
                    addNumber(2);
                    break;
                case '3':
                    addNumber(3);
                    break;
                case '4':
                    addNumber(4);
                    break;
                case '5':
                    addNumber(5);
                    break;
                case '6':
                    addNumber(6);
                    break;
                case '7':
                    addNumber(7);
                    break;
                case '8':
                    addNumber(8);
                    break;
                case '9':
                    addNumber(9);
                    break;
                case '.':
                    addPoint();
                    break;
            }
        }
    }

    public void changeSign() {
        isNegative = !isNegative;
    }

    private abstract class Node {

        abstract TypeNode getType();

        abstract char getSymbolic();
    }

    private class PointNode extends Node {

        @Override
        TypeNode getType() {
            return TypeNode.POINT;
        }

        @Override
        char getSymbolic() {
            return '.';
        }
    }

    private class NumberNode extends Node {

        private final char symbolic;

        NumberNode(int n) {
            if (n < 0 || n > 9) {
                throw new NumberFormatException("Number out fo range [0, 9]");
            }
            char a;
            switch (n) {
                case 0: symbolic = '0';
                    break;
                case 1: symbolic = '1';
                    break;
                case 2: symbolic = '2';
                    break;
                case 3: symbolic = '3';
                    break;
                case 4: symbolic = '4';
                    break;
                case 5: symbolic = '5';
                    break;
                case 6: symbolic = '6';
                    break;
                case 7: symbolic = '7';
                    break;
                case 8: symbolic = '8';
                    break;
                case 9: symbolic = '9';
                    break;
                default:
                    throw new NumberFormatException("Number out fo range [0, 9]");
            }
        }

        @Override
        TypeNode getType() {
            return TypeNode.NUMBER;
        }

        @Override
        char getSymbolic() {
            return symbolic;
        }
    }

    private enum TypeNode {
        NUMBER, POINT;
    }
}
